Template.KEY_CODE = {
  37: 'left',
  38: 'up',
  39: 'right',
  40: 'down',
  13: 'enter',
  27: 'escape'
};


Template.registerHelper('not', function(value) {
  return !value;
});