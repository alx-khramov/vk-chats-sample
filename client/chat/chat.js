Meteor.subscribe("users");
Meteor.subscribe("userData");
Meteor.subscribe("messages");


Template.chat.helpers({
  messages: function () {
    return Messages.find({ groupId: Session.get("selectedGroup") }, { sort: { createdAt: -1 } ,limit: 5 });
  }
});

Template.chat.events({
  'keydown input': function(e) {
    var input = $(e.currentTarget);
    // 


    if (Template.KEY_CODE[e.which] == 'enter' ) {
      if (!input.val().trim().length) return;
      var selectedGroup = Session.get("selectedGroup");
      if (!selectedGroup) return;

      Messages.insert({ 
        createdBy: Meteor.userId(),
        createdAt: new Date(), 
        groupId: selectedGroup,
        text: input.val()
      });

      var groupData = {};
      groupData['groups.' + selectedGroup + '.lastMessage'] = Date.now();
      Meteor.users.update(Meteor.userId(), { $set: groupData });

      input.val('');
    }

    if (Template.KEY_CODE[e.which] == 'escape') {
      input.blur();
    }
  }
});



