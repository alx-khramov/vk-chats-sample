Template.chat_message.helpers({
  user: function(userId) {
    return Meteor.users.findOne(userId);
  },

  time: function(date) {
    return date.toTimeString().split(' ')[0];
  }
});

Template.chat_message.rendered = function() {
  var el = $(this.firstNode);
  var container = $('.component.chat .messages');
  var pos =  container.scrollTop() + el.offset().top + el.height();
  container.animate({
    scrollTop: pos
  }, {
    queue: false
  });
};