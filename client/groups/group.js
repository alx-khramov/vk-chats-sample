

Template.registerHelper('selectedGroup', function() {
  var groupId = Session.get("selectedGroup");
  var user = Meteor.user();
  if (groupId === undefined || !user) return;

  return _.find(Meteor.user().groups, function(g) {
    return g.id == groupId;
  });
});


Template.group.events({
  'click': function(e, template) {
    Session.set("selectedGroup", this.id);
    
    var g = $(e.currentTarget);
    g.siblings().removeClass('selected');
    g.addClass('selected');
  }
});