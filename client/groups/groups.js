Meteor.subscribe("users");
Meteor.subscribe("userData");

Template.groups.helpers({
  groups: function() {
    var user = Meteor.user();
    if (!user || !user.groups) return;

    return _.chain(user.groups).values().sortBy(function(g) {
      return -g.lastMessage || g.vkOrder;
    }).value();
  }
});


Template.groups.events({
  // 'keyup .search': function(e, template) {
  //   var input = $(e.currentTarget);
  // }
});