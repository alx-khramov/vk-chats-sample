Meteor.publish("users", function () {
  return Meteor.users.find({});
});

Meteor.publish("messages", function () {
  return Messages.find({});
});

Meteor.publish("userData", function () {
  if(!this.userId) return null;
  return Meteor.users.find({_id: this.userId}, {fields: {'groups': 1 , 'profile': 1 }});
});